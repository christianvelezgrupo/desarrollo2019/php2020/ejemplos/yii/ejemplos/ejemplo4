﻿CREATE DATABASE jota;
USE jota;

CREATE TABLE productos (
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar (30),
  descripcion varchar (100),
  foto varchar (50),
  oferta boolean
);
DROP TABLE productos;
CREATE TABLE productos (
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar (30),
  descripcion varchar (100),
  foto varchar (50),
  oferta boolean
);

INSERT productos VALUES 
  ('1','patata','Patata de la huerta de murcia 100% orgánico cultivada a bajas temperaturas en metodo de cuelgue durante 5 meses','patata.jpg','true'),
('2','lechuga','Lechuga del municipio de Marcilla de Campos 100% orgánica','lechuga.jpg','false'),
('3','zanahoria','Zanahoria madrileña 100% orgánica cultivada a bajas temperaturas','zanahoria.jpg','false'),
('4','rabano','Rabano leonés 100% orgánico cultivado en pantanos de la serranía de los picos de europa','rabano.jpg','true'),
('5','calabacin','Calabacin cantabro, con mucha agua especial para purés','calabacin.jpg','false');