<?php
use yii\helpers\Html;
?>
<div class="col-lg-6">
    <div class="thumbnail">
      <?= Html::img("@web/imgs/" . $model->foto); ?>
      <div class="caption">
        <h3><?= $model->id ?></h3>
        <p><?= $model->nombre ?></p>
      </div>
    </div>
  </div>
